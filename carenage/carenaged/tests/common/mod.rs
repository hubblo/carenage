use std::env;

pub fn setup() {
    env::set_var(
        "CI_PROJECT_URL",
        "https://gitlab.com/carenage/mywebapplication",
    );
    env::set_var("CI_PROJECT_ID", "54212104651");
    env::set_var("CI_PROJECT_PATH", "hubblo/carenage");
    env::set_var("CI_PIPELINE_ID", "1234");
    env::set_var("CI_PIPELINE_CREATED_AT", chrono::Utc::now().to_rfc3339());
    env::set_var("CI_PIPELINE_NAME", "Pipeline for merge request");
    env::set_var("CI_PIPELINE_URL", "https://gitlab.com/hubblo/carenage/-/pipelines/1637850766");
    env::set_var("CI_JOB_NAME", "build_env_and_test");
    env::set_var("CI_JOB_STAGE", "test");
    env::set_var("CI_JOB_STARTED_AT", chrono::Utc::now().to_rfc3339());
    env::set_var("CI_JOB_ID", "8931381071");
    env::set_var("CI_JOB_URL", "https://gitlab.com/hubblo/carenage/-/jobs/8931381071");
}
