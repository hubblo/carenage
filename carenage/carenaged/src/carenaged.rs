use database::boagent::{
    deserialize_boagent_json, process_embedded_impacts, query_boagent, Config, HardwareData,
};
use database::ci::GitlabVariables;
use database::database::{build_components, collect_processes, get_db_connection_pool, Ids};
use database::metrics::Metrics;
use database::tables::device::{Device, DeviceBuilder};
use database::tables::event::{Event, EventBuilder, EventType};
use database::tables::job::{Job, JobBuilder};
use database::tables::metadata::{Dimension, Metadata, QueryFilter};
use database::tables::pipeline::{Pipeline, PipelineBuilder};
use database::tables::process::{Process, ProcessBuilder};
use database::tables::project::{Project, ProjectBuilder};
use database::tables::run::{Run, RunBuilder};
use database::tables::task::{Task, TaskBuilder};
use database::tables::workflow::{Workflow, WorkflowBuilder};
use database::timestamp::{Timestamp, UnixFlag};
use log::{info, warn};
use std::env;
use std::process;

pub struct DaemonArgs {
    pub time_step: u64,
    pub start_timestamp: Timestamp,
    pub unix_flag: UnixFlag,
}

impl DaemonArgs {
    pub fn parse_args() -> Result<DaemonArgs, Box<dyn std::error::Error>> {
        let args: Vec<String> = env::args().collect();
        let time_step: u64 = args[1].parse()?;
        let start_time_str = args[2].to_string();
        let is_unix_set: bool = args[3].parse()?;
        let unix_flag: UnixFlag = is_unix_set.into();
        let start_timestamp = Timestamp::parse_str(start_time_str, unix_flag);

        info!("All needed daemon arguments are available!");

        Ok(DaemonArgs {
            time_step,
            start_timestamp,
            unix_flag,
        })
    }
}

pub async fn insert_metadata(
    gitlab_vars: GitlabVariables,
    start_timestamp: Timestamp,
    unix_flag: UnixFlag,
    config: &Config,
) -> Result<Ids, Box<dyn std::error::Error>> {
    let db_pool = get_db_connection_pool(&config.database_url).await?;

    let project = ProjectBuilder::new(
        &gitlab_vars.project_path,
        gitlab_vars.project_repo_id,
        &gitlab_vars.project_repo_url,
    )
    .build();

    let insert_project_attempt = Project::try_insert(&project, db_pool.acquire().await?).await?;
    let project_id = Project::retrieve_id(
        insert_project_attempt,
        db_pool.acquire().await?,
        project.name,
        QueryFilter::Name,
    )
    .await?;

    let workflow_name = format!("workflow_{}", gitlab_vars.project_path);
    let workflow = WorkflowBuilder::new(&workflow_name).build();
    let workflow_row = Workflow::insert(&workflow, db_pool.acquire().await?).await?;
    let workflow_id = Workflow::get_id(workflow_row)?;

    let pipeline = PipelineBuilder::new(
        &gitlab_vars.pipeline_name,
        gitlab_vars.pipeline_created_at,
        gitlab_vars.pipeline_id,
        &gitlab_vars.pipeline_url,
    )
    .build();
    let pipeline_insert_attempt = Pipeline::try_insert(&pipeline, db_pool.acquire().await?).await?;
    let pipeline_id = Pipeline::retrieve_id(
        pipeline_insert_attempt,
        db_pool.acquire().await?,
        pipeline.repo_id,
        QueryFilter::RepoId,
    )
    .await?;

    let job = JobBuilder::new(&gitlab_vars.job_name).build();
    let job_row = Job::insert(&job, db_pool.acquire().await?).await?;
    let job_id = Job::get_id(job_row)?;

    let run = RunBuilder::new(
        format!("run_{}", &gitlab_vars.job_name).as_str(),
        gitlab_vars.job_started_at,
        gitlab_vars.job_id,
        &gitlab_vars.job_url,
    )
    .build();
    let run_row = Run::insert(&run, db_pool.acquire().await?).await?;
    let run_id = Run::get_id(run_row)?;

    let task = TaskBuilder::new(&gitlab_vars.job_stage).build();
    let task_row = Task::insert(&task, db_pool.acquire().await?).await?;
    let task_id = Task::get_id(task_row)?;

    let end_time = Timestamp::new(unix_flag);
    let response = query_boagent(
        &config.boagent_url,
        start_timestamp,
        end_time,
        HardwareData::Inspect,
        &config.location,
        config.lifetime,
    )
    .await?;

    let deserialized_boagent_response = deserialize_boagent_json(response).await?;
    let components = build_components(&deserialized_boagent_response)?;
    let device = DeviceBuilder::new(config, components).build();
    let device_row = Device::insert(&device, db_pool.acquire().await?).await?;
    let device_id = Device::get_id(device_row)?;

    let start_process = ProcessBuilder::new(
        process::id() as i32,
        "carenage",
        "carenage start",
        "running",
    )
    .build();

    let process_row = Process::insert(&start_process, db_pool.acquire().await?).await?;
    let process_id = Process::get_id(process_row)?;

    let ids = Ids {
        project_id,
        workflow_id,
        pipeline_id,
        job_id,
        run_id,
        task_id,
        device_id,
        process_id,
    };
    Ok(ids)
}

pub async fn insert_event(
    event: &Event,
    config: &Config,
) -> Result<(), Box<dyn std::error::Error>> {
    let db_pool = get_db_connection_pool(&config.database_url)
        .await?
        .acquire();

    Event::insert(event, db_pool.await?).await?;
    info!("Inserted event data into database.");
    Ok(())
}

pub async fn query_and_insert_event(
    mut ids: Ids,
    start_time: Timestamp,
    unix_flag: UnixFlag,
    fetch_hardware: HardwareData,
    event_type: EventType,
    config: &Config,
) -> Result<(), Box<dyn std::error::Error>> {
    let end_time = Timestamp::new(unix_flag);
    let response = query_boagent(
        &config.boagent_url,
        start_time,
        end_time,
        fetch_hardware,
        &config.location,
        config.lifetime,
    )
    .await?;
    let deserialized_boagent_response = deserialize_boagent_json(response).await?;

    let processes_collection_attempt = collect_processes(&deserialized_boagent_response);

    /* Scaphandre, through Boagent, might not have data on processes available for the timestamps
     * sent by Carenage (notably if all of Boagent / Scaphandre / Carenage are launched at the same
     * time and Carenage is started right away). Handling the Result here and then the Option to
     * cover all possibles cases: there might be some data missing at the launch of Carenage ; or
     * there might be an error during the processing of the request. It could be relevant not to panic
     * here. */

    match processes_collection_attempt {
        Ok(Some(processes)) => {
            for process in processes {
                let db_pool = get_db_connection_pool(&config.database_url).await?;
                let process_metadata_already_registered = Process::check_existence(
                    db_pool.acquire().await?,
                    &process,
                    Dimension::Run,
                    ids.run_id,
                )
                .await?;

                let process_id = match process_metadata_already_registered {
                    true => {
                        Process::search_process_id(
                            db_pool.acquire().await?,
                            &process,
                            Dimension::Run,
                            ids.run_id,
                        )
                        .await?
                    }
                    false => {
                        let process_row =
                            Process::insert(&process, db_pool.acquire().await?).await?;
                        Process::get_id(process_row)?
                    }
                };

                ids.process_id = process_id;

                let event = EventBuilder::new(ids, event_type).build();
                let event_row = Event::insert(&event, db_pool.acquire().await?).await?;
                let event_id = Event::get_id(event_row)?;

                let process_response = process_embedded_impacts(
                    &config.boagent_url,
                    process.pid,
                    start_time,
                    end_time,
                    fetch_hardware,
                    &config.location,
                    config.lifetime,
                )
                .await?;

                let process_data = deserialize_boagent_json(process_response).await?;

                Metrics::build(&process_data, &deserialized_boagent_response)
                    .insert(event_id, db_pool.acquire().await?)
                    .await?;
                info!("Inserted all metrics for query.");
            }
        }
        Ok(None) => info!("No processes data received yet from Scaphandre, carrying on!"),
        Err(_) => warn!(
            "Some error occured while recovering data from Scaphandre, some data might be missing!"
        ),
    }
    info!("Boagent query and metrics insertion attempt over.");
    Ok(())
}
