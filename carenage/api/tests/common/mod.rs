use api::api::AppState;
use axum::{extract::Request, http, Router};
use sqlx::PgPool;
use tokio::net::TcpListener;
use uuid::{uuid, Uuid};

pub struct TestIds {
    pub project_id: Uuid,
    pub pipeline_id: Uuid,
    pub run_id: Uuid,
    pub job_id: Uuid,
    pub workflow_id: Uuid,
    pub task_id: Uuid,
}

pub fn test_ids() -> TestIds {
    TestIds {
        project_id: uuid!("95dfae11-5cad-41d9-bcf9-fa6564c22dd6"),
        pipeline_id: uuid!("9d807f09-e006-4808-9fa2-70f67432d37b"),
        run_id: uuid!("e51076c8-5c47-4a47-a146-04625e77a6ae"),
        job_id: uuid!("6579f658-9286-493e-a3ed-0d92afa09edd"),
        task_id: uuid!("83e9b273-9aa9-4996-8141-751b91aa98b2"),
        workflow_id: uuid!("03c06a5e-a139-4a9e-a770-f69821b10faf"),
    }
}

pub async fn create_server(app: Router) -> String {
    let listener = TcpListener::bind("0.0.0.0:0").await.unwrap();
    let addr = listener.local_addr().unwrap();
    tokio::spawn(async move {
        axum::serve(listener, app).await.unwrap();
    });

    addr.to_string()
}

pub async fn create_response(request: Request) -> http::Response<hyper::body::Incoming> {
    let client = hyper_util::client::legacy::Client::builder(hyper_util::rt::TokioExecutor::new())
        .build_http();

    client.request(request).await.unwrap()
}

pub fn create_state(db_pool: PgPool) -> AppState {
    AppState {
        db_pool,
        gitlabapi_url: "https://test.com".to_string(),
    }
}
