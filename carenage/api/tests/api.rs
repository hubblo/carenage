use std::fs::canonicalize;

use api::api::{
    get_pipeline, get_project, get_run, get_run_with_metrics, AppState, MetricsResponseBuilder,
    PipelineMetadata, ProjectMetadata, RunMetadata, RunWithMetrics,
};
use axum::{
    body::Body,
    http::{Request, StatusCode},
    routing::get,
    Router,
};
use chrono::DateTime;
use database::database::select_metrics_from_dimension;
use database::tables::metadata::Dimension;
use http_body_util::BodyExt;
use mockito::Server;
use sqlx::PgPool;
mod common;
use common::{create_response, create_server, create_state, test_ids};

#[sqlx::test(fixtures("../../database/fixtures/metrics.sql"))]
fn it_formats_all_metrics_received_for_a_given_run_into_api_response_struct(
    pool: PgPool,
) -> sqlx::Result<()> {
    let db_connection = pool.acquire().await?;
    let run_id = test_ids().run_id;

    let project_name = "hubblo/carenage";
    let metrics = select_metrics_from_dimension(db_connection, Dimension::Run, run_id).await?;

    let formatted_response = MetricsResponseBuilder::new(&metrics, project_name).build();

    assert_eq!(formatted_response.processes.len(), 15);
    for process in &formatted_response.processes {
        assert_eq!(process.metrics.len(), 39);
    }
    Ok(())
}

#[sqlx::test(fixtures("../../database/fixtures/metrics.sql"))]
async fn it_returns_a_run_metadata_for_a_given_run_id(db_pool: PgPool) {
    let mock_gitlabapi_response_fp = canonicalize("../mocks/gitlabapi_run_response.json").unwrap();

    let mut mock_server = Server::new_async().await;
    let mock_url = mock_server.url();

    let _mock = mock_server
        .mock("GET", "/projects/58830056/runs/8931381071")
        .with_status(200)
        .with_header("GITLAB-API-TOKEN", "123456")
        .with_body_from_file(mock_gitlabapi_response_fp)
        .create_async()
        .await;

    let state = AppState {
        db_pool,
        gitlabapi_url: mock_url,
    };

    let app = Router::new()
        .route("/projects/:project_id/runs/:run_id", get(get_run))
        .with_state(state);

    let project_id = test_ids().project_id;
    let run_id = test_ids().run_id;

    let server = create_server(app).await;
    let url = format!("http://{server}/projects/{project_id}/runs/{run_id}");

    let request = Request::builder()
        .uri(url)
        .header("GITLAB-API-TOKEN", "123456")
        .body(Body::empty())
        .unwrap();

    let response = create_response(request).await;

    let body = response.into_body().collect().await.unwrap().to_bytes();

    let run_metadata: RunMetadata = serde_json::de::from_slice(&body).unwrap();

    assert_eq!(run_metadata.run.name, "run_build_env");
    assert_eq!(run_metadata.run.id, Some(run_id));
    assert_eq!(
        run_metadata.run.repo_url,
        "https://gitlab.com/hubblo/carenage/-/jobs/8931381071"
    );
    assert_eq!(run_metadata.run.repo_id, 8931381071);
    assert_eq!(
        run_metadata.run.start_date,
        Some(
            DateTime::parse_from_rfc3339("2024-11-05T11:19:22.783871Z")
                .unwrap()
                .to_utc()
        )
    );
    assert_eq!(
        run_metadata.run.stop_date,
        Some(
            DateTime::parse_from_rfc3339("2015-12-24T17:54:31.198Z")
                .unwrap()
                .to_utc()
        )
    );
    assert_eq!(run_metadata.run.duration, Some(0));
    assert_eq!(run_metadata.processes.len(), 16);
    assert_eq!(run_metadata.processes[0].exe, "/usr/local/bin/dockerd");
    assert_eq!(run_metadata.processes[0].pid, 53);
    assert_eq!(run_metadata.processes[0].cmdline, "dockerd--host=unix:///var/run/docker.sock--host=tcp://0.0.0.0:2376--tlsverify--tlscacert/certs/server/ca.pem--tlscert/certs/server/cert.pem--tlskey/certs/server/key.pem");
}

#[sqlx::test(fixtures("../../database/fixtures/metrics.sql"))]
async fn it_returns_a_run_with_its_associated_processes_and_metrics_for_a_given_run_id(
    db_pool: PgPool,
) {
    let state = create_state(db_pool);
    let app = Router::new()
        .route("/runs/:run_id/metrics", get(get_run_with_metrics))
        .with_state(state);

    let run_id = test_ids().run_id;

    let server = create_server(app).await;
    let url = format!("http://{server}/runs/{run_id}/metrics");

    let request = Request::builder().uri(url).body(Body::empty()).unwrap();

    let response = create_response(request).await;

    assert_eq!(response.status(), StatusCode::OK);

    let body = response.into_body().collect().await.unwrap().to_bytes();

    let run_with_metrics: RunWithMetrics = serde_json::de::from_slice(&body).unwrap();

    assert_eq!(run_with_metrics.run_metadata.run.name, "run_build_env");
    assert_eq!(run_with_metrics.run_metadata.run.id, Some(run_id));
    assert_eq!(
        run_with_metrics.run_metadata.run.repo_url,
        "https://gitlab.com/hubblo/carenage/-/jobs/8931381071"
    );
    assert_eq!(run_with_metrics.run_metadata.run.repo_id, 8931381071);
    assert_eq!(run_with_metrics.run_metadata.processes.len(), 16);
    for process in &run_with_metrics.metrics.processes {
        assert_eq!(process.metrics.len(), 39);
    }
}

/* #[sqlx::test(fixtures("../../database/fixtures/metrics.sql"))]
async fn it_returns_an_error_if_database_connection_fails(db_pool: PgPool) {
    // No pool_connection passed to handler. Axum already returns a 500 Internal Servor
    // Error in that case, without a need for explicit error handling. Keeping that test as documentation
    // in case of needing to implement more precise error handling.


    let app = Router::new().route("/runs/:run_id", get(get_run));

    let run_id = test_ids().run_id;

    let url = format!("/runs/{run_id}");

    let request = Request::builder().uri(url).body(Body::empty()).unwrap();
    let response = app.oneshot(request).await.unwrap();

    assert_eq!(response.status(), StatusCode::INTERNAL_SERVER_ERROR);
} */

#[sqlx::test(fixtures("../../database/fixtures/metrics.sql"))]
async fn it_returns_a_project_metadata_for_a_given_project_id(db_pool: PgPool) {
    let mock_gitlabapi_pipeline_response_fp =
        canonicalize("../mocks/gitlabapi_pipeline_response.json").unwrap();

    let mut mock_server = Server::new_async().await;
    let mock_url = mock_server.url();

    let _mock_pipeline_response = mock_server
        .mock("GET", "/projects/58830056/pipelines/1637850769")
        .with_status(200)
        .with_header("GITLAB-API-TOKEN", "123456")
        .with_body_from_file(mock_gitlabapi_pipeline_response_fp)
        .create_async()
        .await;

    let shared_state = AppState {
        db_pool,
        gitlabapi_url: mock_url,
    };

    let app = Router::new()
        .route("/projects/:project_id", get(get_project))
        .with_state(shared_state);

    let project_id = test_ids().project_id;

    let server = create_server(app).await;
    let url = format!("http://{server}/projects/{project_id}");

    let request = Request::builder()
        .uri(url)
        .header("GITLAB-API-TOKEN", "123456")
        .body(Body::empty())
        .unwrap();

    let response = create_response(request).await;

    assert_eq!(response.status(), StatusCode::OK);

    let body = response.into_body().collect().await.unwrap().to_bytes();

    let project_metadata: ProjectMetadata = serde_json::de::from_slice(&body).unwrap();

    assert_eq!(project_metadata.project.name, "hubblo/carenage");
    assert_eq!(project_metadata.project.id, Some(project_id));
    assert_eq!(
        project_metadata.project.repo_url,
        "https://gitlab.com/hubblo/carenage"
    );
    assert_eq!(project_metadata.project.repo_id, 58830056);
    assert_eq!(project_metadata.pipelines.len(), 1);
    assert_eq!(project_metadata.pipelines[0].repo_id, 1637850769);
    assert_eq!(
        project_metadata.pipelines[0].stop_date,
        Some(
            DateTime::parse_from_rfc3339("2022-09-21T01:05:50.175Z")
                .unwrap()
                .to_utc()
        )
    );
    assert_eq!(project_metadata.pipelines[0].duration, Some(34));
}

#[sqlx::test(fixtures("../../database/fixtures/metrics.sql"))]
async fn it_returns_a_pipeline_metadata_for_a_given_pipeline_id(
    db_pool: PgPool,
) -> sqlx::Result<()> {
    let mock_gitlabapi_pipeline_response_fp =
        canonicalize("../mocks/gitlabapi_pipeline_response.json").unwrap();
    let mock_gitlabapi_run_response_fp =
        canonicalize("../mocks/gitlabapi_run_response.json").unwrap();

    let mut mock_server = Server::new_async().await;
    let mock_url = mock_server.url();

    let _mock_pipeline_response = mock_server
        .mock("GET", "/projects/58830056/pipelines/1637850769")
        .with_status(200)
        .with_header("GITLAB-API-TOKEN", "123456")
        .with_body_from_file(mock_gitlabapi_pipeline_response_fp)
        .create_async()
        .await;

    let _mock_run_response = mock_server
        .mock("GET", "/projects/58830056/runs/8931381071")
        .with_status(200)
        .with_header("GITLAB-API-TOKEN", "123456")
        .with_body_from_file(mock_gitlabapi_run_response_fp)
        .create_async()
        .await;

    let shared_state = AppState {
        db_pool,
        gitlabapi_url: mock_url,
    };

    let app = Router::new()
        .route(
            "/projects/:project_id/pipelines/:pipeline_id",
            get(get_pipeline),
        )
        .with_state(shared_state);

    let server = create_server(app).await;

    let project_id = test_ids().project_id;
    let pipeline_id = test_ids().pipeline_id;

    let url = format!("http://{server}/projects/{project_id}/pipelines/{pipeline_id}");

    let request = Request::builder()
        .uri(url)
        .header("GITLAB-API-TOKEN", "123456")
        .body(Body::empty())
        .unwrap();

    let response = create_response(request).await;

    assert_eq!(response.status(), StatusCode::OK);

    let body = response.into_body().collect().await.unwrap().to_bytes();

    let pipeline_metadata: PipelineMetadata = serde_json::de::from_slice(&body).unwrap();

    assert_eq!(
        pipeline_metadata.pipeline.start_date,
        Some(
            DateTime::parse_from_rfc3339("2024-11-05T11:19:22.783871Z")
                .unwrap()
                .to_utc()
        )
    );
    assert_eq!(
        pipeline_metadata.pipeline.stop_date,
        Some(
            DateTime::parse_from_rfc3339("2022-09-21T01:05:50.175Z")
                .unwrap()
                .to_utc()
        )
    );
    assert_eq!(pipeline_metadata.pipeline.duration, Some(34));
    assert_eq!(pipeline_metadata.pipeline.name, "pipeline");
    assert_eq!(pipeline_metadata.pipeline.id, Some(pipeline_id));
    assert_eq!(
        pipeline_metadata.pipeline.repo_url,
        "https://gitlab.com/hubblo/carenage/-/pipelines/1637850769"
    );
    assert_eq!(pipeline_metadata.pipeline.repo_id, 1637850769);
    assert_eq!(pipeline_metadata.runs.len(), 1);
    assert_eq!(pipeline_metadata.runs[0].repo_id, 8931381071);
    assert_eq!(
        pipeline_metadata.runs[0].stop_date,
        Some(
            DateTime::parse_from_rfc3339("2015-12-24T17:54:31.198Z")
                .unwrap()
                .to_utc()
        )
    );
    Ok(())
}
