use api::api::{app, AppState};
use database::boagent::Config;
use log::info;
use sqlx::postgres::PgPoolOptions;
use tokio::net::TcpListener;
use tower_http::trace::{self, TraceLayer};
use tracing::Level;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    let project_root_path = std::env::current_dir().unwrap().join("..");
    let config = Config::check_configuration(&project_root_path)
        .expect("Configuration fields should be parsable.");

    let db_pool = PgPoolOptions::new()
        .max_connections(10)
        .connect(&config.database_url)
        .await
        .expect("Failed to connect to database");

    let shared_state = AppState {
        db_pool,
        gitlabapi_url: "https://gitlab.com/api/v4".to_owned()
    };

    let app = app(shared_state).layer(
        TraceLayer::new_for_http()
            .make_span_with(trace::DefaultMakeSpan::new().level(Level::INFO))
            .on_response(trace::DefaultOnResponse::new().level(Level::INFO)),
    );

    let listener = TcpListener::bind("0.0.0.0:3000").await.unwrap();
    info!("Listening on port 3000!");
    axum::serve(listener, app).await.unwrap();
}
