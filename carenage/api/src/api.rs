use axum::extract::State;
use axum::{debug_handler, extract::Path, response::Json, routing::get, Router};
use chrono::{DateTime, Local};
use database::database::{
    select_metrics_from_dimension, select_project_name_from_dimension, Record,
};
use database::tables::metadata::{Dimension, Metadata, QueryFilter};
use database::tables::pipeline::Pipeline;
use database::tables::process::Process;
use database::tables::project::Project;
use database::tables::run::Run;
use hyper::HeaderMap;
use serde::{Deserialize, Serialize};
use sqlx::{PgPool, Row};
use std::collections::HashSet;
use uuid::Uuid;

#[derive(Clone)]
pub struct AppState {
    pub db_pool: PgPool,
    pub gitlabapi_url: String,
}
#[derive(Deserialize, Serialize)]
pub struct ProjectMetadata {
    pub project: Project,
    pub pipelines: Vec<Pipeline>,
}

#[derive(Deserialize, Serialize)]
pub struct PipelineMetadata {
    pub pipeline: Pipeline,
    pub runs: Vec<Run>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct RunMetadata {
    pub run: Run,
    pub processes: Vec<Process>,
}

#[derive(Clone, Debug, Deserialize, Eq, Hash, Ord, PartialEq, PartialOrd, Serialize)]
pub struct ProcessInfo {
    pub process_pid: i32,
    pub process_exe: String,
    pub process_cmdline: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ProcessMetrics {
    pub metric_name: String,
    pub metric_values: Vec<(DateTime<Local>, f64)>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ProcessRecord {
    pub process: ProcessInfo,
    pub metrics: Vec<ProcessMetrics>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct MetricsResponse {
    pub project_name: String,
    pub processes: Vec<ProcessRecord>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct RunWithMetrics {
    pub project_name: String,
    pub run_metadata: RunMetadata,
    pub metrics: MetricsResponse,
}

pub struct MetricsResponseBuilder(MetricsResponse);

impl MetricsResponseBuilder {
    pub fn new(records: &[Record], project_name: &str) -> Self {
        let mut metrics_names: Vec<String> = records
            .iter()
            .map(|record| record.metric.clone())
            .collect::<HashSet<String>>()
            .into_iter()
            .collect();

        let mut processes_infos: Vec<ProcessInfo> = records
            .iter()
            .map(|record| ProcessInfo {
                process_pid: record.pid,
                process_exe: record.exe.clone(),
                process_cmdline: record.cmdline.clone(),
            })
            .collect::<HashSet<ProcessInfo>>()
            .into_iter()
            .collect();

        processes_infos.sort();
        metrics_names.sort();

        let processes: Vec<ProcessRecord> = processes_infos
            .into_iter()
            .map(|process| {
                let process_metrics: Vec<ProcessMetrics> = metrics_names
                    .clone()
                    .into_iter()
                    .map(|metric_name| {
                        let metric_values = records
                            .iter()
                            .filter(|record| {
                                record.pid == process.process_pid && record.metric == metric_name
                            })
                            .map(|record| (record.timestamp, record.value))
                            .collect::<Vec<(DateTime<Local>, f64)>>();

                        ProcessMetrics {
                            metric_name,
                            metric_values,
                        }
                    })
                    .collect();
                ProcessRecord {
                    process,
                    metrics: process_metrics,
                }
            })
            .collect();
        MetricsResponseBuilder(MetricsResponse {
            project_name: project_name.to_owned(),
            processes,
        })
    }

    pub fn build(self) -> MetricsResponse {
        self.0
    }
}

pub fn get_token(headers: &HeaderMap) -> &str {
    headers
        .get("GITLAB-API-TOKEN")
        .expect(
            "GITLAB-API-TOKEN has not been provided, it is needed to query the Gitlab RESTful API.",
        )
        .to_str()
        .unwrap()
}

#[debug_handler]
pub async fn get_project(
    State(state): State<AppState>,
    Path(project_id): Path<Uuid>,
    headers: HeaderMap,
) -> Json<ProjectMetadata> {
    let project = Project::select(
        state.db_pool.acquire().await.unwrap(),
        &Project::table_name(),
        project_id,
    )
    .await
    .unwrap();

    let pipelines_ids = Project::select_associated_ids(
        state.db_pool.acquire().await.unwrap(),
        &Project::table_name(),
        Dimension::Pipeline,
        project_id,
    )
    .await
    .unwrap();

    futures::future::join_all(pipelines_ids.iter().map(|pipeline_id| async {
        let pipeline_stop_date_is_set = Pipeline::check_field_existence(
            state.db_pool.acquire().await.unwrap(),
            &Pipeline::table_name(),
            *pipeline_id,
            QueryFilter::StopDate,
        )
        .await
        .unwrap();

        if !pipeline_stop_date_is_set {
            let gitlabapi_token = get_token(&headers);
            Pipeline::fetch_and_update(
                state.db_pool.clone(),
                gitlabapi_token,
                &state.gitlabapi_url,
                project_id,
                *pipeline_id,
            )
            .await
            .unwrap();
        }
    }))
    .await;

    let pipelines = Pipeline::select_all(
        state.db_pool.acquire().await.unwrap(),
        Pipeline::table_name(),
        Dimension::Project,
        project_id,
        QueryFilter::StartDate,
    )
    .await
    .unwrap();

    let project_metadata = ProjectMetadata { project, pipelines };
    Json(project_metadata)
}

#[debug_handler]
pub async fn get_pipeline(
    State(state): State<AppState>,
    Path((project_id, pipeline_id)): Path<(Uuid, Uuid)>,
    headers: HeaderMap,
) -> Json<PipelineMetadata> {
    let pipeline_table = Pipeline::table_name();
    let pipeline_stop_date_is_set = Pipeline::check_field_existence(
        state.db_pool.acquire().await.unwrap(),
        &pipeline_table,
        pipeline_id,
        QueryFilter::StopDate,
    )
    .await
    .unwrap();

    if !pipeline_stop_date_is_set {
        let gitlab_token = get_token(&headers);

        Pipeline::fetch_and_update(
            state.db_pool.clone(),
            gitlab_token,
            &state.gitlabapi_url,
            project_id,
            pipeline_id,
        )
        .await
        .unwrap();
    }

    let pipeline = Pipeline::select(
        state.db_pool.acquire().await.unwrap(),
        &pipeline_table,
        pipeline_id,
    )
    .await
    .unwrap();

    let runs_ids = Pipeline::select_associated_ids(
        state.db_pool.acquire().await.unwrap(),
        &Pipeline::table_name(),
        Dimension::Run,
        pipeline_id,
    )
    .await
    .unwrap();

    futures::future::join_all(runs_ids.iter().map(|run_id| async {
        let run_stop_date_is_set = Run::check_field_existence(
            state.db_pool.acquire().await.unwrap(),
            &Run::table_name(),
            *run_id,
            QueryFilter::StopDate,
        )
        .await
        .unwrap();

        if !run_stop_date_is_set {
            let gitlab_token = get_token(&headers);

            Run::fetch_and_update(
                state.db_pool.clone(),
                gitlab_token,
                &state.gitlabapi_url,
                project_id,
                *run_id,
            )
            .await
            .unwrap();
        }
    }))
    .await;

    let runs = Run::select_all(
        state.db_pool.acquire().await.unwrap(),
        Run::table_name(),
        Dimension::Pipeline,
        pipeline_id,
        QueryFilter::StartDate,
    )
    .await
    .unwrap();

    let pipeline_metadata = PipelineMetadata { pipeline, runs };

    Json(pipeline_metadata)
}

#[debug_handler]
pub async fn get_run(
    State(state): State<AppState>,
    Path((project_id, run_id)): Path<(Uuid, Uuid)>,
    headers: HeaderMap,
) -> Json<RunMetadata> {
    let run_stop_date_is_set = Run::check_field_existence(
        state.db_pool.acquire().await.unwrap(),
        &Run::table_name(),
        run_id,
        QueryFilter::StopDate,
    )
    .await
    .unwrap();

    if !run_stop_date_is_set {
        let gitlab_token = get_token(&headers);

        Run::fetch_and_update(
            state.db_pool.clone(),
            gitlab_token,
            &state.gitlabapi_url,
            project_id,
            run_id,
        )
        .await
        .unwrap();
    }

    let run = Run::select(
        state.db_pool.acquire().await.unwrap(),
        &Run::table_name(),
        run_id,
    )
    .await
    .unwrap();

    let processes = Process::select_all(
        state.db_pool.acquire().await.unwrap(),
        Process::table_name(),
        Dimension::Run,
        run_id,
        QueryFilter::ProcessId,
    )
    .await
    .unwrap();

    let run_metadata = RunMetadata { run, processes };

    Json(run_metadata)
}

#[debug_handler]
pub async fn get_run_with_metrics(
    State(state): State<AppState>,
    Path(run_id): Path<Uuid>,
) -> Json<RunWithMetrics> {
    let project_name = select_project_name_from_dimension(
        state.db_pool.acquire().await.unwrap(),
        Dimension::Run,
        run_id,
    )
    .await
    .unwrap()
    .get::<&str, &str>("name")
    .to_owned();

    let run = Run::select(
        state.db_pool.acquire().await.unwrap(),
        &Run::table_name(),
        run_id,
    )
    .await
    .unwrap();

    let processes = Process::select_all(
        state.db_pool.acquire().await.unwrap(),
        Process::table_name(),
        Dimension::Run,
        run_id,
        QueryFilter::ProcessId,
    )
    .await
    .unwrap();

    let run_metadata = RunMetadata { run, processes };

    let metrics_rows = select_metrics_from_dimension(
        state.db_pool.acquire().await.unwrap(),
        Dimension::Run,
        run_id,
    )
    .await
    .unwrap();

    let metrics_response = MetricsResponseBuilder::new(&metrics_rows, &project_name).build();

    let run_with_metrics = RunWithMetrics {
        project_name,
        run_metadata,
        metrics: metrics_response,
    };

    Json(run_with_metrics)
}

pub fn app(state: AppState) -> Router {
    Router::new()
        .route("/", get(|| async { "Welcome to the Carenage API!\n" }))
        .route("/projects/:project_id", get(get_project))
        .route("/projects/:project_id/runs/:run_id", get(get_run))
        .route("/runs/:run_id/metrics", get(get_run_with_metrics))
        .route(
            "/projects/:project_id/pipelines/:pipeline_id",
            get(get_pipeline),
        )
        .with_state(state)
}
