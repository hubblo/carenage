use crate::gitlabapi::query_gitlabapi;
use crate::tables::metadata::{Metadata, Table};
use crate::tables::project::Project;
use chrono::{DateTime, Utc};
use log::info;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use sqlx::pool::PoolConnection;
use sqlx::postgres::{PgQueryResult, PgRow};
use sqlx::types::Uuid;
use sqlx::PgPool;
use sqlx::Postgres;

// An instance of a CI job.
#[derive(Debug, sqlx::FromRow, Serialize, Deserialize)]
pub struct Run {
    pub id: Option<Uuid>,
    pub name: String,
    pub start_date: Option<DateTime<Utc>>,
    pub stop_date: Option<DateTime<Utc>>,
    pub repo_id: i64,
    pub repo_url: String,
    pub duration: Option<i32>,
}

pub struct RunBuilder(Run);

impl RunBuilder {
    pub fn new(name: &str, start_date: DateTime<Utc>, repo_id: i64, repo_url: &str) -> Self {
        RunBuilder(Run {
            id: None,
            name: name.to_owned(),
            start_date: Some(start_date),
            stop_date: None,
            repo_id,
            repo_url: repo_url.to_owned(),
            duration: None,
        })
    }
    pub fn build(self) -> Run {
        self.0
    }
}

impl Metadata for Run {
    async fn insert(
        &self,
        db_connection: PoolConnection<Postgres>,
    ) -> Result<PgRow, sqlx::error::Error> {
        let insert_query =
            "INSERT INTO runs (name, start_date, repo_id, repo_url) VALUES ($1, $2, $3, $4) RETURNING id";

        let process_row = sqlx::query(insert_query)
            .bind(&self.name)
            .bind(self.start_date)
            .bind(self.repo_id)
            .bind(&self.repo_url)
            .fetch_one(&mut db_connection.detach())
            .await?;

        info!("Inserted run metadata into database.");

        Ok(process_row)
    }

    fn table_name() -> Table {
        Table {
            plural: "runs".to_string(),
            singular: "run".to_string(),
        }
    }
}

impl Run {
    pub async fn fetch_and_update(
        db_pool: PgPool,
        gitlab_token: &str,
        gitlabapi_url: &str,
        project_id: Uuid,
        run_id: Uuid,
    ) -> Result<PgQueryResult, Box<dyn std::error::Error>> {
        let project =
            Project::select(db_pool.acquire().await?, &Project::table_name(), project_id).await?;

        let project_repo_id = project.repo_id;

        let run = Run::select(db_pool.acquire().await?, &Self::table_name(), run_id).await?;

        let run_repo_id = run.repo_id;
        let path = format!("/projects/{project_repo_id}/runs/{run_repo_id}");
        let response: Value = serde_json::from_value(
            query_gitlabapi(gitlabapi_url, &path, gitlab_token)
                .await?
                .json()
                .await?,
        )?;

        let stop_date = DateTime::parse_from_rfc3339(
            response
                .get("finished_at")
                .expect("Response from Gitlab API should include finished_at key")
                .as_str()
                .unwrap(),
        )
        .unwrap()
        .to_utc();
        let duration: f64 = response
            .get("duration")
            .expect("Response from Gitlab API should include duration key")
            .as_f64()
            .unwrap();

        let row = Run::update_stop_date_and_duration(
            db_pool.acquire().await?,
            &Run::table_name(),
            run_id,
            stop_date,
            duration as i32,
        )
        .await?;

        Ok(row)
    }
}
