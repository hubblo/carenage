use crate::tables::metadata::InsertAttempt;
use chrono::{DateTime, Utc};
use log::info;
use serde::{Deserialize, Serialize};
use sqlx::pool::PoolConnection;
use sqlx::types::Uuid;
use sqlx::Postgres;

use super::metadata::{Metadata, Table};

#[derive(sqlx::FromRow, Debug, Serialize, Deserialize)]
pub struct Project {
    pub id: Option<Uuid>,
    pub name: String,
    pub created_at: Option<DateTime<Utc>>,
    pub repo_id: i64,
    pub repo_url: String,
}

pub struct ProjectBuilder(Project);

impl ProjectBuilder {
    pub fn new(name: &str, repo_id: i64, repo_url: &str) -> Self {
        ProjectBuilder(Project {
            id: None,
            name: name.to_owned(),
            created_at: None,
            repo_id,
            repo_url: repo_url.to_owned(),
        })
    }
    pub fn build(self) -> Project {
        self.0
    }
}

impl Metadata for Project {
    fn table_name() -> Table {
        Table {
            singular: "project".to_owned(),
            plural: "projects".to_owned(),
        }
    }
    async fn insert(
        &self,
        db_pool: PoolConnection<Postgres>,
    ) -> Result<sqlx::postgres::PgRow, sqlx::error::Error> {
        let insert_query =
            "INSERT INTO projects (name, repo_id, repo_url) VALUES ($1, $2, $3) RETURNING id";

        let project_row = sqlx::query(insert_query)
            .bind(&self.name)
            .bind(self.repo_id)
            .bind(&self.repo_url)
            .fetch_one(&mut db_pool.detach())
            .await?;

        info!("Inserted project metadata into database!");
        Ok(project_row)
    }
}

// Because a project and its metadata might already be present in the database, it is necessary to
// adopt a different behaviour on how to insert and get the UUID of the project. A project
// namespace is unique and attempting to insert the same metadata panicks.  An insertion
// attempt is either succesful (first reference to the project in the database) or not, and the
// daemon must not panick in that case. The UUID is acquired through a different selection query
// with the project namespace as filter.
impl Project {
    pub async fn try_insert(
        &self,
        db_connection: PoolConnection<Postgres>,
    ) -> Result<InsertAttempt, Box<dyn std::error::Error>> {
        let insert_query =
            "INSERT INTO projects (name, repo_id, repo_url) VALUES ($1, $2, $3) RETURNING id";

        let project_row = sqlx::query(insert_query)
            .bind(&self.name)
            .bind(self.repo_id)
            .bind(&self.repo_url)
            .fetch_one(&mut db_connection.detach())
            .await;

        info!("Attempting to insert project metadata into database...");
        Ok(InsertAttempt::Pending(project_row))
    }
}
