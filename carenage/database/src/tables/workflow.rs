use crate::tables::metadata::{Metadata, Table};
use chrono::{DateTime, Utc};
use log::info;
use serde::{Deserialize, Serialize};
use sqlx::pool::PoolConnection;
use sqlx::postgres::PgRow;
use sqlx::types::Uuid;
use sqlx::Postgres;

#[derive(sqlx::FromRow, Serialize, Deserialize)]
pub struct Workflow {
    pub id: Option<Uuid>,
    pub name: String,
    pub start_date: Option<DateTime<Utc>>,
    pub stop_date: Option<DateTime<Utc>>,
}

pub struct WorkflowBuilder(Workflow);

impl WorkflowBuilder {
    pub fn new(name: &str) -> Self {
        WorkflowBuilder(Workflow {
            id: None,
            name: name.to_owned(),
            start_date: None,
            stop_date: None,
        })
    }
    pub fn build(self) -> Workflow {
        self.0
    }
}

impl Metadata for Workflow {
    async fn insert(
        &self,
        db_connection: PoolConnection<Postgres>,
    ) -> Result<PgRow, sqlx::error::Error> {
        let insert_query = "INSERT INTO workflows (name) VALUES ($1) RETURNING id";

        let workflow_row = sqlx::query(insert_query)
            .bind(&self.name)
            .fetch_one(&mut db_connection.detach())
            .await?;

        info!("Inserted workflow metadata into database!");
        Ok(workflow_row)
    }

    fn table_name() -> Table {
        Table {
            plural: "workflows".to_string(),
            singular: "workflow".to_string(),
        }
    }
}
