use crate::tables::metadata::{Metadata, Table};
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use sqlx::pool::PoolConnection;
use sqlx::postgres::PgRow;
use sqlx::types::Uuid;
use sqlx::Postgres;

// A stage in a CI pipeline.
#[derive(sqlx::FromRow, Serialize, Deserialize)]
pub struct Task {
    pub id: Option<Uuid>,
    pub name: String,
    pub start_date: Option<DateTime<Utc>>,
    pub stop_date: Option<DateTime<Utc>>,
}

pub struct TaskBuilder(Task);

impl TaskBuilder {
    pub fn new(name: &str) -> Self {
        TaskBuilder(Task {
            id: None,
            name: name.to_owned(),
            start_date: None,
            stop_date: None,
        })
    }
    pub fn build(self) -> Task {
        self.0
    }
}

impl Metadata for Task {
    async fn insert(&self, db_pool: PoolConnection<Postgres>) -> Result<PgRow, sqlx::error::Error> {
        let insert_query = "INSERT INTO tasks (name) VALUES ($1) RETURNING id";

        let task_row = sqlx::query(insert_query)
            .bind(&self.name)
            .fetch_one(&mut db_pool.detach())
            .await?;

        Ok(task_row)
    }
    fn table_name() -> Table {
        Table {
            plural: "tasks".to_string(),
            singular: "task".to_string(),
        }
    }
}
