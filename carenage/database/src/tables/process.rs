use crate::tables::metadata::{Dimension, Metadata, Table};
use log::info;
use serde::{Deserialize, Serialize};
use sqlx::pool::PoolConnection;
use sqlx::postgres::PgRow;
use sqlx::types::Uuid;
use sqlx::{Postgres, Row};

#[derive(sqlx::FromRow, Clone, Debug, Serialize, Deserialize)]
pub struct Process {
    pub pid: i32,
    pub exe: String,
    pub cmdline: String,
    pub state: String,
}

pub struct ProcessBuilder(Process);

impl ProcessBuilder {
    pub fn new(pid: i32, exe: &str, cmdline: &str, state: &str) -> Self {
        ProcessBuilder(Process {
            pid,
            exe: exe.to_owned(),
            cmdline: cmdline.to_owned(),
            state: state.to_owned(),
        })
    }
    pub fn build(self) -> Process {
        self.0
    }
}

impl Metadata for Process {
    async fn insert(
        &self,
        db_connection: PoolConnection<Postgres>,
    ) -> Result<PgRow, sqlx::error::Error> {
        let insert_query =
            "INSERT INTO processes (pid, exe, cmdline, state) VALUES ($1, $2, $3, $4) RETURNING id";

        let process_row = sqlx::query(insert_query)
            .bind(self.pid)
            .bind(&self.exe)
            .bind(&self.cmdline)
            .bind(&self.state)
            .fetch_one(&mut db_connection.detach())
            .await?;

        info!("Inserted process metadata into database.");

        Ok(process_row)
    }

    fn table_name() -> Table {
        Table {
            plural: "processes".to_string(),
            singular: "process".to_string(),
        }
    }
}

impl Process {
    pub async fn search_process_id(
        database_connection: PoolConnection<Postgres>,
        process: &Process,
        dimension: Dimension,
        dimension_id: Uuid,
    ) -> Result<Uuid, sqlx::Error> {
        let mut connection = database_connection.detach();

        let formatted_query = format!("SELECT PROCESSES.id FROM PROCESSES INNER JOIN EVENTS ON EVENTS.{}_id = ($1) WHERE pid = ($2) AND exe = ($3)", dimension);

        let process_row = sqlx::query(&formatted_query)
            .bind(dimension_id)
            .bind(process.pid)
            .bind(&process.exe)
            .fetch_one(&mut connection)
            .await?;
        let process_id: Uuid = process_row.get("id");
        Ok(process_id)
    }

    pub async fn check_existence(
        database_connection: PoolConnection<Postgres>,
        process: &Process,
        dimension: Dimension,
        dimension_id: Uuid,
    ) -> Result<bool, sqlx::Error> {
        let mut connection = database_connection.detach();
        let formatted_query = format!("SELECT EXISTS (SELECT 1 FROM PROCESSES INNER JOIN EVENTS ON EVENTS.{}_id = ($1) WHERE pid = ($2) AND exe = ($3))", dimension);

        let row = sqlx::query(&formatted_query)
            .bind(dimension_id)
            .bind(process.pid)
            .bind(&process.exe)
            .fetch_one(&mut connection)
            .await?;

        let b = row.get("exists");

        info!(
            "{}",
            match b {
                true => "Process metadata already registered!",
                false => "Process metadata not registered, it will be inserted!",
            }
        );

        Ok(b)
    }
}
