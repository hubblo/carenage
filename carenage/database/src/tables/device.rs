use crate::boagent::Config;
use log::info;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use sqlx::{Postgres, Row};
use sqlx::pool::PoolConnection;
use sqlx::postgres::PgRow;
use sqlx::types::Uuid;

#[derive(sqlx::Type, Debug, serde::Deserialize, serde::Serialize)]
// https://serde.rs/enum-representations.html#untagged
#[serde(untagged)]
pub enum CharacteristicValue {
    StringValue,
    NumericValue,
}

impl CharacteristicValue {
    pub fn to_string(self, value: &Value) -> String {
        match self {
            CharacteristicValue::StringValue => value.as_str().unwrap().to_string(),
            CharacteristicValue::NumericValue => value.as_i64().unwrap().to_string(),
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct Device {
    pub name: String,
    pub location: String,
    pub lifetime: i64,
    pub components: Vec<Component>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Component {
    pub name: String,
    pub model: String,
    pub manufacturer: String,
    pub characteristics: Vec<ComponentCharacteristic>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ComponentCharacteristic {
    pub name: String,
    pub value: String,
}

pub struct DeviceBuilder(Device);
pub struct ComponentBuilder(Component);
pub struct ComponentCharacteristicBuilder(ComponentCharacteristic);

impl DeviceBuilder {
    pub fn new(config: &Config, components: Vec<Component>) -> Self {
        DeviceBuilder(Device {
            name: config.device_name.to_owned(),
            location: config.location.to_owned(),
            lifetime: config.lifetime.into(),
            components,
        })
    }
    pub fn build(self) -> Device {
        self.0
    }
}

impl ComponentBuilder {
    pub fn new(
        name: &str,
        model: &str,
        manufacturer: &str,
        characteristics: Vec<ComponentCharacteristic>,
    ) -> Self {
        ComponentBuilder(Component {
            name: name.to_owned(),
            model: model.to_owned(),
            manufacturer: manufacturer.to_owned(),
            characteristics,
        })
    }
    pub fn build(self) -> Component {
        self.0
    }
}

impl ComponentCharacteristicBuilder {
    pub fn new(name: &str, value: String) -> Self {
        ComponentCharacteristicBuilder(ComponentCharacteristic {
            name: name.to_owned(),
            value,
        })
    }
    pub fn build(self) -> ComponentCharacteristic {
        self.0
    }
}

impl Device {
    pub async fn insert(
        &self,
        db_pool: PoolConnection<Postgres>,
    ) -> Result<PgRow, sqlx::error::Error> {
        let mut connection = db_pool.detach();

        let formatted_query =
            "INSERT INTO devices (name, lifetime, location) VALUES ($1, $2, $3) RETURNING *";
        let device_row = sqlx::query(formatted_query)
            .bind(&self.name)
            .bind(self.lifetime)
            .bind(&self.location)
            .fetch_one(&mut connection)
            .await?;

        let device_id: Uuid = device_row.get("id");
        let formatted_query = "INSERT INTO components (device_id, name, model, manufacturer) VALUES ($1, $2, $3, $4) RETURNING id";
        for component in &self.components {
            let insert_component_data_query = sqlx::query(formatted_query)
                .bind(device_id)
                .bind(component.name.as_str())
                .bind(component.model.as_str())
                .bind(component.manufacturer.as_str())
                .fetch_one(&mut connection)
                .await?;

            info!("Inserted component metadata into database!");

            let component_id: Uuid = insert_component_data_query.get("id");

            let component_characteristics = &component.characteristics;

            for component_characteristic in component_characteristics {
                let formatted_query = "INSERT INTO component_characteristic (component_id, name, value) VALUES ($1, $2, $3)";

                sqlx::query(formatted_query)
                    .bind(component_id)
                    .bind(component_characteristic.name.as_str())
                    .bind(&component_characteristic.value)
                    .execute(&mut connection)
                    .await?;
            }
            info!("Inserted component characteristic metadata into database!");
        }

        info!("Inserted device metadata into database!");

        Ok(device_row)
    }
    pub fn get_id(device_row: PgRow) -> Result<Uuid, sqlx::error::Error> {
        let device_id = device_row.get("id");
        Ok(device_id)
    }
}
