use chrono::{DateTime, Utc};
use crate::tables::metadata::{Metadata, Table};
use serde::{Deserialize, Serialize};
use sqlx::Postgres;
use sqlx::postgres::PgRow;
use sqlx::pool::PoolConnection;
use sqlx::types::Uuid;

#[derive(sqlx::FromRow, Serialize, Deserialize)]
pub struct Job {
    pub id: Option<Uuid>,
    pub name: String,
    pub start_date: Option<DateTime<Utc>>,
    pub stop_date: Option<DateTime<Utc>>,
}

pub struct JobBuilder(Job);

impl JobBuilder {
    pub fn new(name: &str) -> Self {
        JobBuilder(Job {
            id: None,
            name: name.to_owned(),
            start_date: None,
            stop_date: None,
        })
    }
    pub fn build(self) -> Job {
        self.0
    }
}

impl Metadata for Job {
    async fn insert(
        &self,
        db_connection: PoolConnection<Postgres>,
    ) -> Result<PgRow, sqlx::error::Error> {
        let insert_query = "INSERT INTO jobs (name) VALUES ($1) RETURNING id";

        let job_row = sqlx::query(insert_query)
            .bind(&self.name)
            .fetch_one(&mut db_connection.detach())
            .await?;

        Ok(job_row)
    }

    fn table_name() -> Table {
        Table {
            plural: "jobs".to_string(),
            singular: "job".to_string(),
        }
    }
}
