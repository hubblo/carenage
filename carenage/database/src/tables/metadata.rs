use chrono::{DateTime, Utc};
use log::{error, info};
use sqlx::error::ErrorKind;
use sqlx::pool::PoolConnection;
use sqlx::postgres::{PgQueryResult, PgRow, Postgres};
use sqlx::Row;
use std::fmt::Display;
use std::process;
use uuid::Uuid;

pub struct Table {
    pub plural: String,
    pub singular: String,
}

impl Display for Table {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {})", self.plural, self.singular)
    }
}

pub enum Dimension {
    Project,
    Workflow,
    Pipeline,
    Job,
    Run,
    Task,
    Device,
    Component,
    ComponentCharacteristic,
    Container,
    Process,
    Event,
}

impl Display for Dimension {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", &self.as_str())
    }
}

impl Dimension {
    fn as_str(&self) -> &'static str {
        match self {
            Dimension::Project => "project",
            Dimension::Workflow => "workflow",
            Dimension::Pipeline => "pipeline",
            Dimension::Job => "job",
            Dimension::Run => "run",
            Dimension::Task => "task",
            Dimension::Device => "device",
            Dimension::Component => "component",
            Dimension::ComponentCharacteristic => "component_characteristic",
            Dimension::Container => "container",
            Dimension::Process => "process",
            Dimension::Event => "event",
        }
    }
}

pub enum QueryFilter {
    StartDate,
    StopDate,
    ProcessId,
    RepoId,
    Name,
}

impl Display for QueryFilter {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", &self.as_str())
    }
}

impl QueryFilter {
    fn as_str(&self) -> &'static str {
        match self {
            QueryFilter::StartDate => "start_date",
            QueryFilter::StopDate => "stop_date",
            QueryFilter::ProcessId => "pid",
            QueryFilter::RepoId => "repo_id",
            QueryFilter::Name => "name",
        }
    }
}

pub enum InsertAttempt {
    Success(PgRow),
    Pending(Result<PgRow, sqlx::Error>),
}

pub trait Metadata:
    Sized + Unpin + Send + for<'r> sqlx::FromRow<'r, sqlx::postgres::PgRow>
{
    async fn insert(&self, db_pool: PoolConnection<Postgres>) -> Result<PgRow, sqlx::error::Error>;

    fn get_id(row: PgRow) -> Result<Uuid, sqlx::error::Error> {
        let id = row.get("id");

        info!("Got the UUID from inserted database row.");

        Ok(id)
    }

    // To have generic implementations of some query methods without having to write table names, and
    // with possibilities of spelling problems when using singular or plural, using this method to
    // be implemented for each struct to get the correct plural or singular.

    fn table_name() -> Table;

    async fn select(
        db_pool: PoolConnection<Postgres>,
        table: &Table,
        id: Uuid,
    ) -> Result<Self, sqlx::error::Error> {
        let select_query = format!(
            "SELECT * FROM {table_plural} WHERE {table_plural}.id = ($1)",
            table_plural = table.plural
        );

        let row: Self = sqlx::query_as(&select_query)
            .bind(id)
            .fetch_one(&mut db_pool.detach())
            .await?;

        info!(
            "Selected data from '{table_plural}' table with provided UUID.",
            table_plural = table.plural
        );

        Ok(row)
    }

    async fn get_id_with_field<T>(
        db_pool: PoolConnection<Postgres>,
        field: T,
        query_filter: QueryFilter,
    ) -> Result<Uuid, sqlx::error::Error>
    where
        for<'a> T: sqlx::Type<Postgres> + sqlx::Encode<'a, Postgres> + std::marker::Send,
    {
        let table = Self::table_name();
        let mut connection = db_pool.detach();

        let formatted_query = format!(
            "SELECT id FROM {table_plural} WHERE {query_filter} = ($1)",
            table_plural = table.plural
        );

        let row = sqlx::query(&formatted_query)
            .bind(field)
            .fetch_one(&mut connection)
            .await?;

        Ok(row.get("id"))
    }

    async fn retrieve_id<T>(
        insert_attempt: InsertAttempt,
        db_pool: PoolConnection<Postgres>,
        field: T,
        query_filter: QueryFilter,
    ) -> Result<Uuid, sqlx::error::Error>
    where
        for<'a> T: sqlx::Type<Postgres> + sqlx::Encode<'a, Postgres> + std::marker::Send,
    {
        let table = Self::table_name();
        let id: uuid::Uuid = match insert_attempt {
            InsertAttempt::Pending(Ok(row)) => {
                info!(
                    "Inserted {table_singular} metadata into database.",
                    table_singular = table.singular
                );
                row.get("id")
            }
            InsertAttempt::Pending(Err(err)) => {
                info!(
                    "Error while attempting to insert {table_singular} metadata: {err:?}",
                    table_singular = table.singular
                );
                match err
                    .as_database_error()
                    .expect("It should be a DatabaseError.")
                    .kind()
                {
                    ErrorKind::UniqueViolation => {
                        info!("Metadata already present in database: {}.", err);
                        Self::get_id_with_field(db_pool, field, query_filter).await?
                    }
                    _ => {
                        error!("Error while processing metadata insertion: {}", err);
                        process::exit(0x0100)
                    }
                }
            }
            InsertAttempt::Success(row) => {
                info!(
                    "Inserted {table_singular} metadata into database.",
                    table_singular = table.singular
                );
                row.get("id")
            }
        };
        Ok(id)
    }

    async fn select_all(
        db_pool: PoolConnection<Postgres>,
        table: Table,
        dimension: Dimension,
        dimension_id: Uuid,
        query_filter: QueryFilter,
    ) -> Result<Vec<Self>, sqlx::error::Error> {
        let mut connection = db_pool.detach();
        let select_query = format!(
            "SELECT DISTINCT {table_plural}.* FROM {table_plural} INNER JOIN events ON events.{table_singular}_id = {table_plural}.id WHERE events.{dimension}_id = ($1) ORDER BY {table_plural}.{query_filter}", table_plural = table.plural, table_singular = table.singular
        );
        let rows: Vec<Self> = sqlx::query_as(&select_query)
            .bind(dimension_id)
            .fetch_all(&mut connection)
            .await?;

        info!(
            "Selected data from '{table_plural}' table through '{dimension}'.",
            table_plural = table.plural
        );

        Ok(rows)
    }

    async fn check_field_existence(
        db_pool: PoolConnection<Postgres>,
        table: &Table,
        dimension_id: Uuid,
        query_filter: QueryFilter,
    ) -> Result<bool, sqlx::error::Error> {
        let mut connection = db_pool.detach();
        let check_query = format!("SELECT EXISTS (SELECT 1 FROM {table_plural} WHERE {table_plural}.{query_filter} IS NOT NULL AND {table_plural}.id = ($1))", table_plural = table.plural);

        let row = sqlx::query(&check_query)
            .bind(dimension_id)
            .fetch_one(&mut connection)
            .await?;

        let boolean = row.get("exists");

        info!(
            "Checked the existence of queried field from '{table_plural}'.",
            table_plural = table.plural
        );

        Ok(boolean)
    }

    async fn update_stop_date_and_duration(
        db_pool: PoolConnection<Postgres>,
        table: &Table,
        dimension_id: Uuid,
        stop_date: DateTime<Utc>,
        duration: i32,
    ) -> Result<PgQueryResult, sqlx::error::Error> {
        let update_query = format!(
            "UPDATE {table_plural} SET stop_date = ($1), duration = ($2) WHERE id = ($3)",
            table_plural = table.plural
        );

        let query_execution = sqlx::query(&update_query)
            .bind(stop_date)
            .bind(duration)
            .bind(dimension_id)
            .execute(&mut db_pool.detach())
            .await?;

        info!("Updated stop_date and duration columns for this element: {dimension_id}");
        Ok(query_execution)
    }

    async fn select_associated_ids(
        db_connection: PoolConnection<Postgres>,
        table: &Table,
        dimension: Dimension,
        table_id: Uuid,
    ) -> Result<Vec<Uuid>, sqlx::error::Error> {
        let mut connection = db_connection.detach();
        let column = format!("{dimension}_id");
        let select_query = format!(
            "SELECT DISTINCT events.{column} FROM events WHERE events.{table_singular}_id = ($1)",
            table_singular = table.singular
        );

        let rows = sqlx::query(&select_query)
            .bind(table_id)
            .fetch_all(&mut connection)
            .await?;

        let ids = rows
            .into_iter()
            .map(|row| row.get(column.as_str()))
            .collect();
        Ok(ids)
    }
}
