use crate::tables::device::{CharacteristicValue, Component, ComponentBuilder, ComponentCharacteristicBuilder};
use crate::tables::metadata::Dimension;
use crate::tables::process::{Process, ProcessBuilder};
use chrono::{DateTime, Local};
use serde_json::{Error, Value};
use sqlx::pool::PoolConnection;
use sqlx::postgres::PgRow;
use sqlx::types::uuid::Uuid;
use sqlx::{PgPool, Postgres};

#[derive(Copy, Clone)]
pub struct Ids {
    pub project_id: Uuid,
    pub workflow_id: Uuid,
    pub pipeline_id: Uuid,
    pub job_id: Uuid,
    pub run_id: Uuid,
    pub task_id: Uuid,
    pub device_id: Uuid,
    pub process_id: Uuid,
}

#[derive(sqlx::FromRow, Debug)]
pub struct Record {
    pub timestamp: DateTime<Local>,
    pub pid: i32,
    pub exe: String,
    pub cmdline: String,
    pub metric: String,
    pub value: f64,
}

pub async fn get_db_connection_pool(database_url: &str) -> Result<PgPool, sqlx::Error> {
    let connection_pool = PgPool::connect(database_url);

    connection_pool.await
}

pub fn to_datetime_local(timestamp_str: &str) -> chrono::DateTime<Local> {
    DateTime::parse_from_str(timestamp_str, "%Y-%m-%d %H:%M:%S%.9f %:z")
        .expect("It should be a parsable string to be converted to an ISO8601 timestamp with local timezone.").into()
}

pub fn build_components(deserialized_boagent_response: &Value) -> Result<Vec<Component>, Error> {
    let hardware_data = &deserialized_boagent_response["raw_data"]["hardware_data"];

    let mut components = vec![];

    let cpus = hardware_data["cpus"]
        .as_array()
        .expect("Unable to parse CPUs JSON array from Boagent.")
        .iter();
    let rams = hardware_data["rams"]
        .as_array()
        .expect("Unable to parse RAM JSON array from Boagent.")
        .iter();
    let disks = hardware_data["disks"]
        .as_array()
        .expect("Unable to parse Disks JSON array from Boagent.")
        .iter();

    let cpu_components_iter = cpus.map(|cpu| {
        let core_units = ComponentCharacteristicBuilder::new(
            "core_units",
            CharacteristicValue::NumericValue.to_string(&cpu["core_units"]),
        )
        .build();
        ComponentBuilder::new(
            "cpu",
            cpu["name"].as_str().unwrap(),
            cpu["manufacturer"].as_str().unwrap(),
            vec![core_units],
        )
        .build()
    });

    components.extend(cpu_components_iter);

    let ram_components_iter = rams.map(|ram| {
        let capacity = ComponentCharacteristicBuilder::new(
            "capacity",
            CharacteristicValue::NumericValue.to_string(&ram["capacity"]),
        )
        .build();
        ComponentBuilder::new(
            "ram",
            "not implemented",
            ram["manufacturer"].as_str().unwrap(),
            vec![capacity],
        )
        .build()
    });

    components.extend(ram_components_iter);

    let disk_components_iter = disks.map(|disk| {
        let capacity = ComponentCharacteristicBuilder::new(
            "capacity",
            CharacteristicValue::NumericValue.to_string(&disk["capacity"]),
        )
        .build();
        let disk_type = ComponentCharacteristicBuilder::new(
            "type",
            CharacteristicValue::StringValue.to_string(&disk["type"]),
        )
        .build();
        ComponentBuilder::new(
            "disk",
            "not implemented",
            disk["manufacturer"].as_str().unwrap(),
            vec![capacity, disk_type],
        )
        .build()
    });

    components.extend(disk_components_iter);

    Ok(components)
}

/* Boagent might return an empty array for ["raw_data"]["power_data"]["raw_data"]
* due to one of the first /query requests being too early
* while Scaphandre did not yet record data on processes relevant for the timestamps sent to
* Boagent. Instead of panicking, it might be acceptable to return None and log the absence
* of data for processes for that request. */

pub fn collect_processes(
    deserialized_boagent_response: &Value,
) -> Result<Option<Vec<Process>>, Error> {
    let processes: Option<Vec<Process>> = deserialized_boagent_response["raw_data"]["power_data"]
        ["raw_data"]
        .as_array()
        .expect("Boagent response should be parsable")
        .last()
        .map(|boagent_response| {
            boagent_response
                .get("consumers")
                .expect("Consumers should be available from Scaphandre.")
                .as_array()
                .expect("Consumers should contain information on processes.")
                .iter()
                .map(|process| {
                    ProcessBuilder::new(
                        process["pid"]
                            .as_i64()
                            .expect("Process ID should be an integer.")
                            as i32,
                        process["exe"].as_str().expect("Exe should be available."),
                        process["cmdline"]
                            .as_str()
                            .expect("Cmdline should be available."),
                        "running",
                    )
                    .build()
                })
                .collect()
        });

    Ok(processes)
}

pub async fn select_metrics_from_dimension(
    database_connection: PoolConnection<Postgres>,
    dimension: Dimension,
    dimension_id: Uuid,
) -> Result<Vec<Record>, sqlx::Error> {
    let mut connection = database_connection.detach();

    let formatted_query = format!(
        "SELECT DISTINCT events.timestamp, processes.pid, processes.exe, processes.cmdline, processes.id, metrics.metric, metrics.value FROM PROCESSES INNER JOIN EVENTS ON events.process_id = processes.id INNER JOIN METRICS ON metrics.event_id = events.id WHERE events.{}_id=($1) ORDER BY processes.id, events.timestamp, metrics.metric",
        dimension
    );

    let records: Vec<Record> = sqlx::query_as(&formatted_query)
        .bind(dimension_id)
        .fetch_all(&mut connection)
        .await?;

    Ok(records)
}

pub async fn select_project_name_from_dimension(
    database_connection: PoolConnection<Postgres>,
    dimension: Dimension,
    dimension_id: Uuid,
) -> Result<PgRow, sqlx::Error> {
    let mut connection = database_connection.detach();

    let formatted_query = format!("SELECT DISTINCT projects.name FROM PROJECTS INNER JOIN EVENTS ON events.project_id = projects.id WHERE events.{}_id=($1)", dimension);

    let project_row = sqlx::query(&formatted_query)
        .bind(dimension_id)
        .fetch_one(&mut connection)
        .await?;

    Ok(project_row)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::boagent::Config;
    use chrono::Local;
    use std::io::Write;

    #[test]
    fn it_checks_that_all_needed_configuration_details_are_set() -> std::io::Result<()> {
        let current_dir = std::env::current_dir().unwrap();
        let config_path = current_dir.join("../mocks/").canonicalize().unwrap();
        let env_file = config_path.join(".env");
        let mut config_file = std::fs::File::create(env_file.clone())
            .expect("Failed to create env file for testing.");
        config_file.write_all(
            b"DATABASE_URL='postgres://carenage:password@localhost:5432/carenage'
PROJECT_NAME='carenage_webapp'
BOAGENT_URL='http://localhost:8000'
LOCATION='FRA'
LIFETIME=5
",
        )?;
        let config_check = Config::check_configuration(env_file.as_path());

        assert!(config_check.is_ok());
        Ok(())
    }

    #[test]
    fn it_converts_a_parsable_string_containing_an_iso8601_timestamp_to_a_datetime_with_local_offset(
    ) {
        let dt_local_timestamp = Local::now();
        let converted_string = to_datetime_local(dt_local_timestamp.to_string().as_str());
        assert_eq!(dt_local_timestamp, converted_string);
    }
}
