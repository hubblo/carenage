use std::env;
use log::info;
use chrono::{DateTime, Utc};

pub struct GitlabVariables {
    pub project_repo_url: String,
    pub project_repo_id: i64,
    pub project_path: String,
    pub pipeline_id: i64,
    pub pipeline_created_at: DateTime<Utc>,
    pub pipeline_name: String,
    pub pipeline_url: String,
    pub job_id: i64,
    pub job_name: String,
    pub job_stage: String,
    pub job_started_at: DateTime<Utc>,
    pub job_url: String,
}

impl GitlabVariables {
    pub fn parse_env_variables() -> Result<GitlabVariables, Box<dyn std::error::Error>> {
        let project_repo_url = env::var("CI_PROJECT_URL")?.to_string();
        let project_repo_id = env::var("CI_PROJECT_ID")?.to_string().parse::<i64>()?;
        let project_path = env::var("CI_PROJECT_PATH")?.to_string();
        let pipeline_id = env::var("CI_PIPELINE_ID")?.to_string().parse::<i64>()?;
        let pipeline_created_at = DateTime::parse_from_rfc3339(&env::var("CI_PIPELINE_CREATED_AT")?)?.to_utc();
        let pipeline_name = env::var("CI_PIPELINE_NAME")?.to_string();
        let pipeline_url = env::var("CI_PIPELINE_URL")?.to_string();
        let job_id = env::var("CI_JOB_ID")?.to_string().parse::<i64>()?;
        let job_name = env::var("CI_JOB_NAME")?.to_string();
        let job_stage = env::var("CI_JOB_STAGE")?.to_string();
        let job_started_at = DateTime::parse_from_rfc3339(&env::var("CI_JOB_STARTED_AT")?)?.to_utc();
        let job_url = env::var("CI_JOB_URL")?.to_string();

        info!("All needed Gitlab variables are available!");

        Ok(GitlabVariables {
            project_repo_url,
            project_repo_id,
            project_path,
            pipeline_id,
            pipeline_created_at,
            pipeline_name,
            pipeline_url,
            job_id,
            job_name,
            job_stage,
            job_started_at,
            job_url,
        })
    }
}
