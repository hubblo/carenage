use reqwest::{Client, Response};

pub async fn query_gitlabapi(
    url: &str,
    path: &str,
    token: &str,
) -> Result<Response, reqwest::Error> {
    let client = Client::new();
    let formatted_url = format!("{url}{path}");
    client
        .get(formatted_url)
        .header("PRIVATE-TOKEN", token)
        .send()
        .await
}

#[cfg(test)]
mod tests {
    use serde_json::Value;
    use std::fs::canonicalize;

    use super::*;
    use mockito::Server;

    #[sqlx::test]
    async fn it_queries_the_gitlab_rest_api_to_get_stop_date_and_duration_for_a_pipeline() {
        let mut server = Server::new_async().await;
        let url = server.url();
        let token = "123456";
        let path = "/projects/1/pipelines/46";

        let mock_gitlab_api_response_fp =
            canonicalize("../mocks/gitlabapi_pipeline_response.json").unwrap();

        let _mock = server
            .mock("GET", "/projects/1/pipelines/46")
            .with_status(200)
            .with_header("PRIVATE-TOKEN", token)
            .with_body_from_file(mock_gitlab_api_response_fp)
            .create_async()
            .await;

        let response = query_gitlabapi(&url, path, token).await;

        assert!(response.is_ok());

        let response_json: Value =
            serde_json::from_value(response.unwrap().json().await.unwrap()).unwrap();
        assert_eq!(response_json["finished_at"], "2022-09-21T01:05:50.175Z");
        assert_eq!(response_json["duration"], 34);
    }
}
