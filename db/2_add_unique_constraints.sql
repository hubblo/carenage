ALTER TABLE pipelines ADD CONSTRAINT pipeline_repo_id_is_unique UNIQUE (repo_id);
ALTER TABLE projects ADD CONSTRAINT project_repo_id_is_unique UNIQUE (repo_id);
ALTER TABLE runs ADD CONSTRAINT run_repo_id_is_unique UNIQUE (repo_id);
